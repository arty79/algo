package lesson40.user;

/**
 * Created by a.panasyuk on 29.06.2018.
 */
public class Work {
    private String boss;

    public String getBoss() {
        return boss;
    }

    public void setBoss(String boss) {
        this.boss = boss;
    }
}