package lesson32.jsonexample;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

/* Десериализация JSON объекта
НЕОБХОДИМО: подключенные библиотеки Jackson Core, Bind и Annotation версии 2.6.1

В метод convertFromJsonToNormal приходит имя файла, который содержит один ДЖЕЙСОН объект.
Метод convertFromJsonToNormal должен вычитать объект из файла, преобразовать его из JSON и вернуть его.
*/
public class DeClazz {

    public static Cat convertFromJsonToNormal(String fileName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(fileName), Cat.class);
    }

    public static void main(String[] args) {
        String json = "JsonFileExample.txt";
        try {
            Cat cat = convertFromJsonToNormal(json);
            System.out.println(cat);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Pet {
        @JsonProperty
        String name;
    }

    public static class Cat extends Pet {

        @JsonProperty
        int age;
        @JsonProperty
        int weight;

        @Override
        public String toString() {
            return "Cat{" +
                    "name=" + name +
                    ", age=" + age +
                    ", weight=" + weight +
                    '}';
        }
    }
}