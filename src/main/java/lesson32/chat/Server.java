package lesson32.chat;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by arty on 02.11.2015.
 */
public class Server {
    private static Map<String, Connection> connectionMap = new ConcurrentHashMap<>();


    private static class Handler extends Thread {
        @Override
        public void run() {

            String errorMessage = "Произошла ошибка обмена данных с удаленным адресом: " + socket.getRemoteSocketAddress();
            String userName = null;
            SocketAddress socketAddress = null;
            try (Connection connection = new Connection(socket)) {
                socketAddress = connection.getRemoteSocketAddress();
                ConsoleHelper.writeMessage("установлено новое соединение с удаленным адресом" + socketAddress);
                userName = serverHandshake(connection);
                sendBroadcastMessage(new Message(MessageType.USER_ADDED, userName));
                sendListOfUsers(connection, userName);
                serverMainLoop(connection, userName);
            } catch (IOException e) {
                ConsoleHelper.writeMessage(errorMessage);
            } catch (ClassNotFoundException e) {
                ConsoleHelper.writeMessage(errorMessage);
            } finally {
                if (userName != null) {
                    connectionMap.remove(userName);
                    sendBroadcastMessage(new Message(MessageType.USER_REMOVED, userName));
                }
            }
            ConsoleHelper.writeMessage("закрыто новое соединение с удаленным адресом" + socketAddress);

        }

        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        private String serverHandshake(Connection connection) throws IOException, ClassNotFoundException {
            while (true) {
                Message message_request = new Message(MessageType.NAME_REQUEST);
                connection.send(message_request);
                Message message_response = connection.receive();
                if (message_response.getType() == MessageType.USER_NAME) {
                    String name = message_response.getData();
                    if (name != null && !name.isEmpty()) {
                        if (!connectionMap.containsKey(name)) {
                            connectionMap.put(name, connection);
                            Message message = new Message(MessageType.NAME_ACCEPTED);
                            connection.send(message);
                            return name;
                        }
                    }
                }
            }
        }

        private void sendListOfUsers(Connection connection, String userName) throws IOException {
            for (Map.Entry<String, Connection> pair : connectionMap.entrySet()) {
                if (!pair.getKey().equals(userName)) {
                    Message message = new Message(MessageType.USER_ADDED, pair.getKey());
                    connection.send(message);
                }
            }
        }

        private void serverMainLoop(Connection connection, String userName) throws IOException, ClassNotFoundException {
            while (true) {
                Message message = connection.receive();
                if (message.getType() == MessageType.TEXT) {
                    sendBroadcastMessage(new Message(MessageType.TEXT, String.format("%s: %s", userName, message.getData())));
                } else {
                    ConsoleHelper.writeMessage("неверный формат сообщения");
                }
            }
        }
    }

    public static void sendBroadcastMessage(Message message) {
        for (Map.Entry<String, Connection> pair : connectionMap.entrySet()) {
            try {
                pair.getValue().send(message);
            } catch (IOException e) {
                e.printStackTrace();
                ConsoleHelper.writeMessage("Don't send message to " + pair.getKey());
            }
        }
    }

    public static void main(String[] args) {
        ConsoleHelper.writeMessage("Input server port");
        try (ServerSocket serverSocket = new ServerSocket(ConsoleHelper.readInt())) {
            System.out.println("Server run");
            while (true) {
                Handler handler = new Handler(serverSocket.accept());
                handler.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error!");
        }

    }

}
