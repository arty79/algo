package lesson32.chat;

/**
 * Created by arty on 02.11.2015.
 */
public enum MessageType {
	NAME_REQUEST,
	USER_NAME,
	NAME_ACCEPTED,
	TEXT,
	USER_ADDED,
	USER_REMOVED;

}
