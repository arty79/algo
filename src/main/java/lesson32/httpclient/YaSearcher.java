package lesson32.httpclient;


import java.io.*;
import java.net.URL;
import java.net.URLEncoder;

public class YaSearcher {

    private static final String YA_URL = "https://yandex.ru/search/?";
    private static final String ENC = "UTF-8";
    private static final String AND = "&";

    /**
     * Constructor
     */
    public YaSearcher() {
    }

    /**
     * Retrieve Yandexresponse stream via GET request
     *
     * @param query search query
     * @return Yandex response stream
     * @throws IOException input/output exception
     */
    public InputStream retrieveResponseViaGetRequest(String query) throws IOException {

        StringBuilder address = new StringBuilder(YA_URL);
        address
                .append("text=")
                .append(URLEncoder.encode(query, ENC))
                .append(AND);
        URL url = new URL(address.toString());
        return url.openStream();
    }

    /**
     * Example: print response to System.out
     */
    public static void main(String[] args) throws IOException {

        String query = "Привет, мир!";

        BufferedReader bufferedReader = null;
        try {
            YaSearcher searcher = new YaSearcher();
            bufferedReader = new BufferedReader(
                    new InputStreamReader(searcher.retrieveResponseViaGetRequest(query)));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
    }
}