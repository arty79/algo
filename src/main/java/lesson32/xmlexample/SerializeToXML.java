package lesson32.xmlexample;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by arty on 12.06.2018.
 */
public class SerializeToXML {
    public static void main(String[] args) throws JAXBException, IOException {
        //создание объекта для сериализации в XML
        Cat cat = new Cat();
        cat.name = "Murka";
        cat.age = 5;
        cat.weight = 4;
        //писать результат сериализации будем в Writer(FileWriter)
        FileWriter fr = new FileWriter(new File("XmlExample.xml"));
        //создание объекта Marshaller, который выполняет сериализацию
        JAXBContext context = JAXBContext.newInstance(Cat.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        // сама сериализация в поток с записью в файл
        marshaller.marshal(cat, fr);
        System.out.println("Serialization finished!");
    }
}
