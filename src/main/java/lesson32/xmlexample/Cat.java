package lesson32.xmlexample;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by arty on 12.06.2018.
 */
@XmlType(name = "cat")
@XmlRootElement
public class Cat{
    public String name;
    public int age;
    public int weight;

    public Cat() {
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                '}';
    }
}