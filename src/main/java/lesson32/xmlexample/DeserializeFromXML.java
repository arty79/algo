package lesson32.xmlexample;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by arty on 12.06.2018.
 */
public class DeserializeFromXML {

    public static void main(String[] args) throws JAXBException, IOException {
        String xmldata = "XmlExample.xml";
        FileReader reader = new FileReader(new File(xmldata));

        JAXBContext context = JAXBContext.newInstance(Cat.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        Cat cat = (Cat) unmarshaller.unmarshal(reader);
        System.out.println(cat);
    }

}
