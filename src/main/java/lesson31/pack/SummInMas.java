package lesson31.pack;


import lesson31.utils.ArrayGenerator;
import lesson31.utils.PrintUtil;

import java.util.Arrays;
import java.util.Scanner;

public class SummInMas {

    //дан очень большой массив целых чисел
    //дано очень большое целое число
    //найти в массиве 2 таких числа,
    //сумма которых будет равна числу на вход
    public static void main(String[] args) {

        ArrayGenerator arrayGenerator =
                new ArrayGenerator();

        int[] mas = arrayGenerator.generateFromConsole();
        PrintUtil.printIntMas(mas);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число!");
        int number = scanner.nextInt();

        System.out.println("2 pointer!");
        long startTime = System.currentTimeMillis();
        twoPointer(mas, number);
        System.out.println("Заняло времени " +
                                (System.currentTimeMillis()
                                            - startTime));

        System.out.println("Binary razn!");
        startTime = System.currentTimeMillis();
        raznostBinary(mas, number);
        System.out.println("Заняло времени " +
                (System.currentTimeMillis()
                        - startTime));
    }

    public static void raznostBinary(int[] mas, int number) {
        Arrays.sort(mas);
        for (int i = 0; i < mas.length; i++) {
            int current = mas[i];
            int forSearch = number - current;
            int foundIndex = Arrays.binarySearch(mas, forSearch);
            if(foundIndex != -1) {
                System.out.println("Found!");
                System.out.println("i=" + i);
                System.out.println("one=" + current);
                System.out.println("j=" + foundIndex);
                System.out.println("two=" + forSearch);
                return;
            }
        }
    }

    public int binarySearch(int[] mas, int forSearch) {
        if(mas.length == 1) {
            return Integer.MIN_VALUE;
        }
        int seredina;
        int seredinaIndex;
        if(mas.length % 2 == 0) {
            seredinaIndex = (mas.length / 2) + 1;
        } else {
            seredinaIndex = mas.length / 2;
        }
        seredina = mas[seredinaIndex];
        if(seredina == forSearch) {
            return seredina;
        } else {
            if(seredina > forSearch) {
                int[] polovinaLittle =  new int[seredinaIndex+1];
                System.arraycopy(mas,
                                0,
                                 polovinaLittle,
                                 0,
                                  polovinaLittle.length);
                return binarySearch(polovinaLittle, forSearch);
            } else {
                int[] polovonaBig = new int[mas.length - seredinaIndex];
                System.arraycopy(mas,
                                 seredinaIndex,
                                polovonaBig,
                                0,
                                polovonaBig.length
                                );

                return binarySearch(polovonaBig, forSearch);
            }
        }


    }

    public static void raznost(int[] mas, int number) {
        Arrays.sort(mas);
        for (int i = 0; i < mas.length; i++) {
            int current = mas[i];
            int forSearch = number - current;
            for (int j = 0; j < mas.length; j++) {
                if(mas[j] == forSearch) {
                    System.out.println("Found!");
                    System.out.println("i=" + i);
                    System.out.println("one=" + current);
                    System.out.println("j=" + j);
                    System.out.println("two=" + forSearch);
                }
            }
        }
    }

    public static void twoPointer(int[] mas, int number) {
        Arrays.sort(mas);
        int currentStart = mas[0];
        int currentEnd = mas[mas.length -1];
        int currentStartIndex = 0;
        int currentEndIndex = mas.length - 1;
        int sum = 0;

        while (currentStartIndex != currentEndIndex) {
            sum = currentStart + currentEnd;
            if(sum == number) {
                System.out.println("Found!");
                System.out.println("i=" + currentStartIndex);
                System.out.println("one=" + currentStart);
                System.out.println("j=" + currentEndIndex);
                System.out.println("two=" + currentEnd);
                return;
            } else {
                if(sum > number) {
                    currentEndIndex--;
                    currentEnd = mas[currentEndIndex];
                } else {
                    currentStart++;
                    currentStart = mas[currentStartIndex];
                }
            }
        }
    }

    public static void vLob(int[] mas, int number) {
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                int sum = mas[i] + mas[j];
                if(sum == number && i != j) {
                    System.out.println("Found!");
                    System.out.println("i=" + i);
                    System.out.println("j=" + j);
                    return;
                }
            }
        }
    }
}
