package lesson31.pack;

import java.util.*;

/**
 * Created by arty on 05.06.2018.
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        int result = check(sc.nextLine());
        if(result == 0) {
            System.out.println("Success");
        } else{
            System.out.println(result);
        }
    }

    public static int check(String input) {
        Stack theStack = new Stack();
        int count = 1;
        List<Character> collect = new ArrayList<>();
        for (int j = 0; j < input.length(); j++) {
            Character ch = input.charAt(j);
            collect.add(ch);
            switch (ch) {
                case '{':
                case '[':
                case '(':
                    theStack.push(ch);
                    break;
                case '}':
                case ']':
                case ')':
                    if (!theStack.isEmpty()) {
                        Character chx = (Character) theStack.pop();
                        if (ch == '}' && chx != '{' || ch == ']' && chx != '[' || ch == ')' && chx != '(') {
                            return count;
                        }
                    } else {
                        return count;
                    }
                    break;
                default:
                    break;
            }
            count+=1;
        }
        if (!theStack.isEmpty()) {
            return  collect.lastIndexOf(theStack.peek())+1;
        }
        return 0;
    }
}

