package lesson31.pack;

import lesson31.utils.ArrayGenerator;
import lesson31.utils.PrintUtil;

/**
 * Created by arty on 06.06.2018.
 */
public class QuickSort {
    private static int nElems;
    private static int[] mas;

    public static void main(String[] args) {
        ArrayGenerator generator =
                new ArrayGenerator();
        mas = generator.generateMas(10, 2, 10);
        nElems = mas.length;
        System.out.println("Initial array: ");
        PrintUtil.printIntMas(mas);
        quickSort(mas);
        System.out.println();
        System.out.println("Sorted array: ");
        PrintUtil.printIntMas(mas);
    }

    private static void quickSort(int[] mas) {
        recQuickSort(0, nElems - 1);
    }

    public static void recQuickSort(int left, int right) {
        if (right - left <= 0) {
            return;
        } else {
            int pivot = mas[right];
            int partition = partitionIt(left, right, pivot);
            recQuickSort(left, partition - 1);
            recQuickSort(partition + 1, right);
        }
    }

    public static int partitionIt(int left, int right, int pivot) {
        int leftPtr = left - 1;
        int rightPtr = right;
        while (true) {
            while (mas[++leftPtr] < pivot) ;
            while (rightPtr > 0 && mas[--rightPtr] > pivot) ;
            if (leftPtr >= rightPtr) {
                break;
            } else {
                swap(leftPtr, rightPtr);
            }
        }
        swap(leftPtr, right);
        return leftPtr;
    }

    private static void swap(int left, int right) {
        int temp = mas[left];
        mas[left] = mas[right];
        mas[right] = temp;
    }
}
