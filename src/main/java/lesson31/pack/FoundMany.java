package lesson31.pack;


import lesson31.utils.ArrayGenerator;
import lesson31.utils.PrintUtil;

import java.util.Arrays;
import java.util.Scanner;

public class FoundMany {
    //найти в массиве N целых чисел
    //M наименьших чисел
    //N > M
    //Исходный  2 1 5 6 7 3 4 8
    //M= 5
    //Результат- 1 2 3 4 5
    public static void main(String[] args) {
        ArrayGenerator generator =
                new ArrayGenerator();
        int[] mas = generator.generateUnniq(10, 2, 10);
        PrintUtil.printIntMas(mas);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число искомых наименьших");
        int seacableMins = scanner.nextInt();

        long startTime = System.currentTimeMillis();
        //vLob(mas, seacableMins);
        //kycha(mas, seacableMins);
        //System.out.println(ranking(mas, 0, mas.length-1, 1));
        rankingMas(mas, seacableMins);
        System.out.println("Заняло времени="
                + (System.currentTimeMillis()
                - startTime)
        );
    }

    //O(nlog(n))
    public static void vLob(int[] mas, int seacableMins) {
        Arrays.sort(mas);
        for (int i = 0; i < seacableMins; i++) {
            System.out.print(mas[i] + " ");
            if (i % 20 == 0) {
                System.out.println();
            }
        }
    }

    public static void kycha(int[] mas, int seacableMins) {
        int[] kycha = new int[seacableMins];
        System.arraycopy(mas,
                0,
                kycha,
                0,
                seacableMins);

        for (int i = seacableMins;
             i < mas.length;
             i++) {
            int tempElemFromMas = mas[i];
            int maxElemKychaIndex = searchMaxElemIndexInKucha(kycha);

            if (tempElemFromMas < kycha[maxElemKychaIndex]) {
                kycha[maxElemKychaIndex] = tempElemFromMas;
            }
        }

        PrintUtil.printIntMas(kycha);
    }

    private static int searchMaxElemIndexInKucha(int[] kycha) {
        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < kycha.length; i++) {
            if (kycha[i] > max) {
                max = kycha[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    public static int partition(int[] array, int left, int right, int pivot) {
        while (true) {
            while (left <= right && array[left] <= pivot)
                left++;
            while (right >= left && array[right] > pivot)
                right--;
            if (left > right)
                return left - 1;
            swap(array, left, right);
        }
    }

    private static void swap(int[] array, int left, int right) {
        int temp = array[left];
        array[left] = array[right];
        array[right] = temp;
    }

    public static int rank(int[] array, int left, int right, int rank) {
        // fix random
        int pivot = rand(left, right);
        int leftend = partition(array, left, right, array[pivot]);

        int size = leftend - left + 1;
        if (size == rank + 1) {
            for(int i=0; i<=leftend; i++) {
                System.out.print(array[i]);
            }
            System.out.println();
            return 0;
            //return min(array, left, right);
        } else if (size > rank)
            return rank(array, left, leftend, rank);
        else return rank(array, leftend + 1, right, rank - size);

    }

/*    private static int min(int[] array, int left, int right) {
        int min = Integer.MAX_VALUE;
        for (int i = left; i < right; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }

        return min;
    }*/

    public static int rand(int left, int right) {
        return left + (int) (Math.random() * (double) (right - left + 1));
    }

    public static void rankingMas(int[] mas, int m) {
        //fix: i < m replace i<=m
        for (int i = 1; i < m; i++) {
            rank(mas, 0, mas.length - 1, i);
        }
    }
}
