package lesson31.pack;

/**
 * Created by arty on 07.06.2018.
 */
public class Spiral2 {
    public static void main(String[] args) {

        int max = 9;
        int r = 1;
        int x = 0;
        int y = 0;
        int[][] m = new int[max][max];
        float border = max - 0.6f;

        for (int d = 0; d < 2 * max - 1; border -= d++ == 0 ? 0f : 0.5f) {
            for (int i = 0; i < Math.round(border); i++) {
                m[d % 4 == 1 ? x++ : d % 4 == 3 ? x-- : x][d % 4 == 0 ? y++ : d % 4 == 2 ? y-- : y] = r++;
            }
        }
        m[x][y] = r;
        for (int i = 0; i < max; i++) {
            for (int j = 0; j < max; j++) {
                System.out.print("  " + m[i][j] + "  ");
            }
            System.out.println();
        }
    }
}
