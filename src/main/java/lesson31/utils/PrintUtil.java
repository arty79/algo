package lesson31.utils;

public class PrintUtil {
    public static void printIntMas(int[] mas) {
        int currentIndex = 0;
        for (int temp:
             mas) {
            System.out.print(temp + " ");
            currentIndex++;
            if(currentIndex % 20 == 0) {
                System.out.println();
            }
        }
        System.out.println();
    }
}
