package lesson31.utils;

import java.util.Random;
import java.util.Scanner;

public class ArrayGenerator {
    private final Random random =
            new Random();

    public int[] generateMas(int lenght,
                              int min,
                              int max) {
        int[] generated = new int[lenght];
        for (int i = 0;
             i < generated.length;
             i++) {
            generated[i] = rand(min,max);
                    //random.nextInt(max - min) + min;
        }

        return generated;
    }

    public static int rand(int left, int right) {
        return left + (int) (Math.random() * (double) (right - left + 1));
    }

    public int[] generateFullRandom() {
        int lenght = random.nextInt(10);
        int min = random.nextInt();
        int max = random.nextInt() + random.nextInt(min / 10);

        return generateMas(lenght, min, max);
    }

    public int[] generateUnniq(int lenght,
                             int min,
                             int max) {
        int[] generated = new int[lenght];
        int rand = random.nextInt(max);
        for (int i = 0;
             i < generated.length;
             i++) {
            generated[i] = i + rand;
        }

        return generated;
    }

    public int[] generateFromConsole() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите длину");
        int lenght = scanner.nextInt();

        System.out.println("Введите минимальный элемент");
        int min = scanner.nextInt();

        System.out.println("Введите максимальный элемент");
        int max = scanner.nextInt();

        if(min > max) {
            System.out.println("Минимальный элемент должен быть меньше максимального!");
            return generateFromConsole();
        }

        return generateMas(lenght, min, max);
    }
}
