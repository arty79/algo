package lesson29;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
  public static Map<String, Person> createMap()
  {
    Map<String, Person> book = new HashMap<>();
    Person person1 = new Person(29,"Петрова","жен");
    Person person2 = new Person(34, "Сидорова", "жен");
    Person person3 = new Person(34, "Тихонова", "жен");
    Person person4 = new Person(35, "Петров", "муж");
    book.put("Key1", person1);
    book.put("Key2", person1);
    book.put("Key3", person2);
    book.put("Key4", person3);
    book.put("Key5", person4);
    book.put("Key6", person4);
    return book;
  }

  public static void main(String[] args)
  {
    Map<String, Person> book = createMap();
    removeTheFirstNameDuplicates(book);
    System.out.println(book.values());
  }

  public static void removeTheFirstNameDuplicates(Map<String, Person> map)

  {
    List<Person> list = new ArrayList<Person>(map.values());
    Set<Person> set = new HashSet<>();

    for(Person s:list)
    {
      if(set.contains(s))
        removeItemFromMapByValue(map,s);
      else set.add(s);
    }
    System.out.println(set);
  }


  public static void removeItemFromMapByValue(Map<String, Person> map, Person value)
  {
    Map<String, Person> copy = new HashMap<>(map);
    for (Map.Entry<String, Person> pair: copy.entrySet())
    {
      if (pair.getValue().equals(value))
        map.remove(pair.getKey());
    }
  }
}
