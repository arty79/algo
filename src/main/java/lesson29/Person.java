package lesson29;

/**
 * Created by a.panasyuk on 20.06.2018.
 */
public class Person {
    private int age;
    private String sex;
    private String firstName;

    public Person() {
    }

    public Person(int age, String sex, String firstName) {
        this.age = age;
        this.sex = sex;
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        if (sex != null ? !sex.equals(person.sex) : person.sex != null) return false;
        return firstName != null ? !firstName.equals(person.firstName) : person.firstName != null;
    }

    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
            "age=" + age +
            ", sex='" + sex + '\'' +
            ", firstName='" + firstName + '\'' +
            '}';
    }
}
