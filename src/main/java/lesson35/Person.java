package lesson35;

/**
 * Created by a.panasyuk on 20.06.2018.
 */
public class Person {
    private int year;
    private int age;
    private String sex;
    private String firstName;
    private String lastName;
    private int inn;
    private int passport;

    public Person() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (year != person.year) return false;
        if (age != person.age) return false;
        if (inn != person.inn) return false;
        if (passport != person.passport) return false;
        if (sex != null ? !sex.equals(person.sex) : person.sex != null) return false;
        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        return lastName != null ? lastName.equals(person.lastName) : person.lastName == null;
    }

    @Override
    public int hashCode() {
        int result = year;
        result = 31 * result + age;
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + inn;
        result = 31 * result + passport;
        return result;
    }
}
