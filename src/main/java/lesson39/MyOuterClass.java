package lesson39;

import java.util.ArrayList;

/**
 * Created by a.panasyuk on 29.06.2018.
 */
class OuterDemo {
    private int num = 42;
    // Внутренний класс
    public class InnerDemo {
        private int num = 24;
        public void print() {
            System.out.println("Это внутренний класс и ему доступно private поле внешнего класса!");
            System.out.println(num);
            System.out.println(OuterDemo.this.num);
        }
    }
    // Доступ к внутреннему классу из метода
    void displayInner() {
        InnerDemo inner = new InnerDemo();
        inner.print();
    }
}
public class MyOuterClass {
    public static void main(String args[]) {
        // Создание внешнего класса
        OuterDemo outer = new OuterDemo();
        // Доступ к методу displayInner()
        outer.displayInner();
        OuterDemo.InnerDemo innerDemo = outer.new InnerDemo();
        innerDemo.print();
    }
}
