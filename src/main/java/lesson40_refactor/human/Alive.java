package lesson40_refactor.human;

/**
 * Created by arty on 06.11.2015.
 */
public interface Alive
{
    void live();
}
