package lesson40_refactor.human;

/**
 * Created by user on 06.11.2015.
 */
public class Soldier extends Human implements Alive
{
    protected int course;

    public Soldier(String name, int age)
    {
        super(name, age);

    }

    public void live()
    {
        fight();
    }

    public void fight()
    {
    }

    public int getCourse()
    {
        return course;
    }
}
