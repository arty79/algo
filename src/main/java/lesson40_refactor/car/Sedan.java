package lesson40_refactor.car;

/**
 * Created by arty on 03.12.2015.
 */
public class Sedan extends Car
{


    public Sedan(int numberOfPassengers)
    {
        super(Car.SEDAN, numberOfPassengers);
    }

    @Override
    public int getMaxSpeed()
    {
        final int MAX_SEDAN_SPEED = 120;
        return MAX_SEDAN_SPEED;
    }
}
